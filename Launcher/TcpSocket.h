#pragma once

class TcpSocket
{
	SOCKET mSocket;

	class PlatformInvoker
	{
	public:
		PlatformInvoker();
	};

	static PlatformInvoker gInvoker;

	std::wstring mAddress;
	uint16 mPort;
	bool mIsConnected;
	SOCKADDR_IN mRemoteAddr;

public:
	TcpSocket(const std::wstring& ip, uint16 port);

	bool tryConnect(uint32& errorCode);

	void send(const void* bytes, uint32 numBytes);
	void receive(void* bytes, uint32 numBytes);

	void readToEnd(std::vector<uint8>& destData);

	template<typename T>
	void send(const T& value) {
		send(&value, sizeof(T));
	}

	template<typename T, uint32 size>
	void send(const T (&value)[size]) {
		send(value, size * sizeof(T));
	}

	template<typename T>
	void send(const std::vector<T>& v) {
		if (v.size() == 0) {
			return;
		}

		send(v.data(), v.size() * sizeof(T));
	}

	template<typename T>
	T receive() {
		T ret;
		receive(&ret, sizeof(T));
		return ret;
	}

	template<typename T, uint32 size>
	void receive(T (&value)[size]) {
		receive(value, size * sizeof(T));
	}

	template<typename T>
	void receive(std::vector<T>& v) {
		if (v.size() == 0) {
			return;
		}

		receive(v.data(), v.size() * sizeof(T));
	}
};

SHARED_TYPE(TcpSocket);