#include "stdafx.h"
#include "DataLoader.h"
#include "Window.h"
#include "HttpRequest.h"
#include "resource.h"

void DataLoader::initLoading() {
	HttpRequestPtr req = std::make_shared<HttpRequest>(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/LauncherVersion.txt");
	sWindow->setLoadProgress(0, 100);
	sWindow->setLoadAction(L"Retrieving launcher version from server...");
	req->asyncGetResponse(std::bind(&DataLoader::onLauncherVersion, this, std::placeholders::_1));
}

void DataLoader::onLauncherVersion(std::wstring version) {
	std::wstringstream strm;
	strm << version;
	uint32 vnr = 0;
	if (strm >> vnr) {
		wchar_t strBuffer[256] = { L'\0' };
		if (LoadString(GetModuleHandle(nullptr), 102, strBuffer, 256) == 0) {
			downloadLauncher();
		} else {
			strm = std::wstringstream();
			strm << strBuffer;
			uint32 localVer = 0;
			if (!(strm >> localVer) || localVer != vnr) {
				downloadLauncher();
			}
		}
	}

	HttpRequestPtr req = std::make_shared<HttpRequest>(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/Changelog.txt");
	sWindow->setLoadProgress(25, 100);
	sWindow->setLoadAction(L"Retrieving changelog from server...");
	req->asyncGetResponse(std::bind(&DataLoader::onChangelog, this, std::placeholders::_1));
}

void DataLoader::onChangelog(std::wstring changeLog) {
	sWindow->setLoadProgress(50, 100);
	sWindow->setLoadAction(L"Retrieving version information from server...");
	sWindow->pushSyncAction([changeLog]() {
		SetDlgItemText(sWindow->getHandle(), IDC_RICHEDIT21, changeLog.c_str());
	});

	HttpRequestPtr req = std::make_shared<HttpRequest>(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/VersionInfo.txt");
	req->asyncGetResponse(std::bind(&DataLoader::onVersionInfo, this, std::placeholders::_1));
}

void DataLoader::onVersionInfo(std::wstring info) {
	static std::wstring libraryFiles[] = {
		L".\\awesomium.dll",
		L".\\bin\\awesomium.dll",
		L".\\bin\\avcodec-53.dll",
		L".\\bin\\avformat-53.dll",
		L".\\bin\\awesomium_pak_utility.exe",
		L".\\bin\\awesomium_process.exe",
		L".\\bin\\icudt.dll",
		L".\\bin\\libEGL.dll",
		L".\\bin\\libGLESv2.dll",
		L".\\bin\\xinput9_1_0.dll"
	};

	sWindow->setLoadProgress(66, 100);
	sWindow->setLoadAction(L"Checking local file version...");

	bool fileMissing = false;

	for (auto& file : libraryFiles) {
		if (GetFileAttributes(file.c_str()) == INVALID_FILE_ATTRIBUTES) {
			fileMissing = true;
			break;
		}
	}

	if (fileMissing == true) {
		downloadLibraries();
	}

	std::wstringstream infostrm;
	infostrm << info;
	uint32 version = 0;
	infostrm >> version;

	if (GetFileAttributes(L"WildstarStudio.exe") == INVALID_FILE_ATTRIBUTES) {
		downloadBuild();
	} else {
		HMODULE hMod = LoadLibraryEx(L"WildstarStudio.exe", nullptr, LOAD_LIBRARY_AS_DATAFILE | LOAD_LIBRARY_AS_IMAGE_RESOURCE);
		if (hMod == nullptr) {
			downloadBuild();
		} else {
			wchar_t strBuffer[256] = { L'\0' };

			if (LoadString(hMod, 101, strBuffer, 256) == 0) {
				downloadBuild();
			} else {
				std::wstringstream strm;
				strm << strBuffer;
				uint32 localVersion = 0;
				if (!(strm >> localVersion) || localVersion != version) {
					downloadBuild();
				}
			}
		}
	}

	sWindow->setLoadProgress(100, 100);
	sWindow->setLoadAction(L"Wildstar Studio is ready to be used!");
	sWindow->pushSyncAction([]() { EnableWindow(GetDlgItem(sWindow->getHandle(), IDC_BUTTON1), TRUE); });
}

void DataLoader::downloadBuild() {
	sWindow->setLoadProgress(0, 100);
	sWindow->setLoadAction(L"Downloading release_build.rar...");

	wchar_t tempPath[MAX_PATH + 1] = { L'\0' };
	GetTempPath(MAX_PATH, tempPath);
	GetTempFileName(tempPath, L"wss", 0, tempPath);

	std::ofstream rarOut(tempPath, std::ios::binary);

	auto lastUpdate = std::chrono::high_resolution_clock::now();

	HttpRequestPtr req = std::make_shared<HttpRequest>(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/release_build.rar");
	req->setRawCallback([&rarOut](uint8* data, uint32 numBytes) {
		rarOut.write((const char*) data, numBytes);
	});

	req->getResponseSyncBinary([this, &lastUpdate](double cur, double total) {
		if (total < 1.0) {
			return;
		}

		auto now = std::chrono::high_resolution_clock::now();
		uint64 diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastUpdate).count();
		if (diff < 200) {
			return;
		}

		lastUpdate = now;

		uint32 pct = (uint32) ((cur / total) * 100.0);
		if (pct > 100) {
			pct = 100;
		}

		sWindow->setLoadProgress(pct);
	});

	sWindow->setLoadProgress(100, 100);

	rarOut.close();

	if (GetFileAttributes(L".\\UI") == FILE_ATTRIBUTE_DIRECTORY) {
		MoveFile(L".\\UI", L".\\UI_backup");
	}

	sWindow->setLoadAction(L"Extracting release_build.rar...");

	RAROpenArchiveDataEx oad = { 0 };
	oad.OpenMode = RAR_OM_EXTRACT;
	oad.ArcNameW = tempPath;

	HANDLE hArchive = RAROpenArchiveEx(&oad);

	if (oad.OpenResult != ERAR_SUCCESS || hArchive == nullptr) {
		throw std::exception("Unable to open rar archive!");
	}

	RARHeaderDataEx fileData = { 0 };
	while (RARReadHeaderEx(hArchive, &fileData) == 0) {
		std::wstring file = fileData.FileNameW;

		RARProcessFileW(hArchive, RAR_EXTRACT, L".\\", fileData.FileNameW);
	}

	RARCloseArchive(hArchive);
	DeleteFile(tempPath);
}

void DataLoader::downloadLibraries() {
	sWindow->setLoadProgress(0, 100);
	sWindow->setLoadAction(L"Downloading awesomium_library.rar...");

	wchar_t tempPath[MAX_PATH + 1] = { L'\0' };
	GetTempPath(MAX_PATH, tempPath);
	GetTempFileName(tempPath, L"wss", 0, tempPath);

	std::ofstream rarOut(tempPath, std::ios::binary);

	auto lastUpdate = std::chrono::high_resolution_clock::now();

	HttpRequestPtr req = std::make_shared<HttpRequest>(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/awesomium_library.rar");
	req->setRawCallback([&rarOut](uint8* data, uint32 numBytes) {
		rarOut.write((const char*) data, numBytes);
	});

	req->getResponseSyncBinary([this, &lastUpdate](double cur, double total) {
		if (total < 1.0) {
			return;
		}
		
		auto now = std::chrono::high_resolution_clock::now();
		uint64 diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastUpdate).count();
		if (diff < 200) {
			return;
		}

		lastUpdate = now;

		uint32 pct = (uint32) ((cur / total) * 100.0);
		if (pct > 100) {
			pct = 100;
		}

		sWindow->setLoadProgress(pct);
	});

	sWindow->setLoadProgress(100, 100);

	rarOut.close();

	sWindow->setLoadAction(L"Extracting awesomium_library.rar...");

	RAROpenArchiveDataEx oad = { 0 };
	oad.OpenMode = RAR_OM_EXTRACT;
	oad.ArcNameW = tempPath;

	HANDLE hArchive = RAROpenArchiveEx(&oad);
	
	if (oad.OpenResult != ERAR_SUCCESS || hArchive == nullptr) {
		throw std::exception("Unable to open rar archive!");
	}

	RARHeaderDataEx fileData = { 0 };
	while (RARReadHeaderEx(hArchive, &fileData) == 0) {
		std::wstring file = fileData.FileNameW;

		RARProcessFileW(hArchive, RAR_EXTRACT, L".\\", fileData.FileNameW);
	}

	RARCloseArchive(hArchive);
	DeleteFile(tempPath);
}

void DataLoader::downloadLauncher() {
	wchar_t tempPath[MAX_PATH + 1] = { L'\0' };
	GetTempPath(MAX_PATH, tempPath);

	std::wstringstream fileStrm;
	fileStrm << tempPath << L"\\" << L"Launcher_new.exe";

	std::ofstream rarOut(fileStrm.str(), std::ios::binary);

	auto lastUpdate = std::chrono::high_resolution_clock::now();

	HttpRequestPtr req = std::make_shared<HttpRequest>(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/Launcher.exe");
	req->setRawCallback([&rarOut](uint8* data, uint32 numBytes) {
		rarOut.write((const char*) data, numBytes);
	});

	req->getResponseSyncBinary([this, &lastUpdate](double cur, double total) {
		if (total < 1.0) {
			return;
		}

		auto now = std::chrono::high_resolution_clock::now();
		uint64 diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastUpdate).count();
		if (diff < 200) {
			return;
		}

		lastUpdate = now;

		uint32 pct = (uint32) ((cur / total) * 100.0);
		if (pct > 100) {
			pct = 100;
		}

		sWindow->setLoadProgress(pct);
	});

	rarOut.close();

	sWindow->setLoadProgress(100, 100);

	wchar_t curDir[MAX_PATH] = { L'\0' };
	GetCurrentDirectory(MAX_PATH, curDir);

	std::wstringstream paramStrm;
	paramStrm << L"--update \"" << curDir << L"\"";
	ShellExecute(nullptr, L"open", fileStrm.str().c_str(), paramStrm.str().c_str(), tempPath, SW_SHOW);
	ExitProcess(0);
}