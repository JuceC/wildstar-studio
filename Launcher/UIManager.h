#pragma once

#include "Window.h"
#include "ResourceFileManager.h"
#include "JSHandler.h"

SHARED_FWD(UIManager);

class UIManager : public Awesomium::WebViewListener::View
{
	static UIManagerPtr gInstance;

	WindowPtr mWindow;
	uint32 mWidth;
	uint32 mHeight;
	Awesomium::WebCore* mCore;
	Awesomium::WebView* mView;
	Awesomium::WebSession* mSession;
	ResourceFileManager mResFileMgr;
	JSHandlerPtr mJSHandler;

	GLuint mTexture;

	virtual void OnChangeTitle(Awesomium::WebView* caller, const Awesomium::WebString& title) { }
	virtual void OnChangeAddressBar(Awesomium::WebView* caller, const Awesomium::WebURL& url) { }
	virtual void OnChangeTooltip(Awesomium::WebView* caller, const Awesomium::WebString& tooltip) { }
	virtual void OnChangeTargetURL(Awesomium::WebView* caller, const Awesomium::WebURL& url) { }
	virtual void OnChangeCursor(Awesomium::WebView* caller, Awesomium::Cursor cursor) { }
	virtual void OnChangeFocus(Awesomium::WebView* caller, Awesomium::FocusedElementType focused_type) { }
	virtual void OnAddConsoleMessage(Awesomium::WebView* caller, const Awesomium::WebString& message, int line_number, const Awesomium::WebString& source);
	virtual void OnShowCreatedWebView(Awesomium::WebView* caller, Awesomium::WebView* new_view, const Awesomium::WebURL& opener_url, const Awesomium::WebURL& target_url, const Awesomium::Rect& initial_pos, bool is_popup) { }

public:
	UIManager();

	void init(uint32 width, uint32 height, WindowPtr wnd);
	void shutdown();

	void onFrame();

	static UIManagerPtr getInstance() {
		if (gInstance == nullptr) {
			gInstance = std::make_shared<UIManager>();
		}

		return gInstance;
	}
};

#define sUIMgr (UIManager::getInstance())