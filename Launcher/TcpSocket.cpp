#include "stdafx.h"
#include "TcpSocket.h"

TcpSocket::PlatformInvoker TcpSocket::gInvoker;

TcpSocket::PlatformInvoker::PlatformInvoker() {
	WSADATA wsd;
	WSAStartup(MAKEWORD(2, 0), &wsd);
}

TcpSocket::TcpSocket(const std::wstring& ip, uint16 port) {
	mAddress = ip;
	mPort = port;
	mIsConnected = false;
}

bool TcpSocket::tryConnect(uint32& errorCode) {
	errorCode = ERROR_SUCCESS;

	if (mIsConnected) {
		return true;
	}

	mSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (mSocket == INVALID_SOCKET) {
		errorCode = WSAGetLastError();
		return false;
	}

	memset(&mRemoteAddr, 0, sizeof(mRemoteAddr));
	mRemoteAddr.sin_family = AF_INET;
	mRemoteAddr.sin_port = htons(mPort);
	
	ULONG addr = inet_addr(String::toAnsi(mAddress).c_str());
	if (addr == INADDR_NONE) {
		hostent* entry = gethostbyname(String::toAnsi(mAddress).c_str());
		if (entry == nullptr || entry->h_length == 0) {
			errorCode = WSAGetLastError();
			return false;
		}

		addr = *(ULONG*) entry->h_addr_list[0];
	}

	mRemoteAddr.sin_addr.s_addr = addr;

	LONG result = connect(mSocket, (const sockaddr*) &mRemoteAddr, sizeof(mRemoteAddr));
	if (result == SOCKET_ERROR) {
		errorCode = WSAGetLastError();
		return false;
	}

	return true;
}

void TcpSocket::send(const void* data, uint32 numBytes) {
	uint8* bytes = (uint8*) data;

	uint32 numSent = 0;
	while (numSent < numBytes) {
		uint32 toSend = numBytes - numSent;
		int32 result = ::send(mSocket, (const char*) (bytes + numSent), toSend, 0);
		if (result <= 0) {
			throw std::exception("Error sending data");
		}

		numSent += static_cast<uint32>(result);
	}
}

void TcpSocket::receive(void* data, uint32 numBytes) {
	uint8* bytes = (uint8*) data;

	uint32 numRecv = 0;
	while (numRecv < numBytes) {
		uint32 toRecv = numBytes - numRecv;
		int32 result = ::recv(mSocket, (char*) (bytes + numRecv), toRecv, 0);
		if (result <= 0) {
			throw std::exception("Error receiving data");
		}

		numRecv += static_cast<uint32>(result);
	}
}

void TcpSocket::readToEnd(std::vector<uint8>& destData) {
	__declspec(thread) static uint8 chunk[0x1000];

	bool didRead = false;
	do {
		int32 ret = ::recv(mSocket, (char*) chunk, 0x1000, 0);
		if (ret <= 0) {
			didRead = false;
		} else {
			destData.insert(destData.end(), chunk, chunk + ret);
		}
	} while (didRead);
}