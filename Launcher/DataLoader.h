#pragma once

class DataLoader
{
private:
	std::wstring mChangelog;

	void onChangelog(std::wstring changelog);
	void onVersionInfo(std::wstring info);
	void downloadLibraries();
	void downloadBuild();
	void downloadLauncher();
	void onLauncherVersion(std::wstring version);
public:
	void initLoading();
};