#pragma once

namespace File {
	std::wstring getExtension(std::wstring file);
	Awesomium::WebString getExtension(const Awesomium::WebString& file);
}