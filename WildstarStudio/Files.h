#pragma once

#include "V8Instance.h"

SHARED_FWD(FileEntry);
SHARED_FWD(BinaryStream);

class File
{
	FileEntryPtr mFile;
	std::vector<uint8> mContent;
	std::unique_ptr<BinStream> mStream;

	void assertStream();
	void setValue(ObjectPtr obj, const std::wstring& name, const std::wstring& type);

public:
	File(std::wstring path);

	FileEntryPtr getFile();
	uint64 getFileSize();
	std::wstring getFileName();
	ObjectPtr read(ArrayPtr scheme);
	uint32 readUInt32();
	int32 readInt32();
	float readFloat();
	double readDouble();
	std::vector<uint8> readBytes(uint32 numBytes);

	void seek(int32 mod);
	uint64 tell();

	std::vector<uint8>& getContent() { assertStream(); return mContent; }
};

class DiscFile
{
	std::ofstream mFile;
	bool mIsBinary;
public:
	DiscFile(std::wstring fileName, bool binary = false);
	~DiscFile();

	void write(ValuePtr value);
	void writeLine(ValuePtr value);

	void writeBinary(BinaryStreamPtr stream);
};

SHARED_TYPE(File);

class FileLibrary
{
public:
	static void onRegister(Scope& scope);
};