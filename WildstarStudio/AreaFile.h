#pragma once

#include "IOManager.h"
#include "BinStream.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Program.h"
#include "Matrix.h"

#pragma pack(push, 1)

struct AreaChunk
{
	uint32 flags;
	uint16 heightMap[19][19];
	uint32 textureLayers[4];
	uint16 blend[65][65];
	uint16 colorMap[65][65];
	uint16 unk1[40];
	uint8 shadowMap[65][65];
};

struct AreaVertex
{
	float x, y, z;
	uint8 r, g, b, a;
};

#pragma pack(pop)

class AreaChunkRender
{
	static std::vector<uint32> indices;

	VertexBufferPtr mVertexBuffer;
	IndexBufferPtr mIndexBuffer;
	AreaChunk mData;
	float mMaxHeight = -FLT_MAX;
	float mAverageHeight = 0.0f;
	std::vector<AreaVertex> mVertices;

public:
	AreaChunkRender(const AreaChunk& chunk, float baseX, float baseY);

	float getMaxHeight() const { return mMaxHeight; }
	float getAverageHeight() const { return mAverageHeight; }

	void exportToObj(std::ofstream& os, uint32& baseIndex);

	void render();
};

SHARED_TYPE(AreaChunkRender);

class AreaFile
{
	static uint32 gUniformView, gUniformProj;

	FileEntryPtr mFile;
	std::shared_ptr<BinStream> mStream;
	std::vector<uint8> mContent;
	std::vector<AreaChunkRenderPtr> mChunks;
	std::wstring mPath;
	std::wstring mModelName;
	float mMaxHeight = -FLT_MAX;
	float mAverageHeight = 0.0f;

	void initGeometry();

public:
	AreaFile(const std::wstring& name, FileEntryPtr file);
	bool load();

	void render(const Matrix& matView, const Matrix& matProj);

	float getMaxHeight() const { return mMaxHeight; }
	float getAverageHeight() const { return mAverageHeight; }
	void exportToObj();

	static const float UnitSize;
};

SHARED_TYPE(AreaFile);