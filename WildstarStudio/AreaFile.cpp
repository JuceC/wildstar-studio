#include "stdafx.h"
#include "AreaFile.h"
#include "InputGeometry.h"
#include "Pipeline.h"
#include "UIManager.h"

const float AreaFile::UnitSize = 1.0f;
InputGeometryPtr gAreaGeom = nullptr;
uint32 AreaFile::gUniformProj = 0;
uint32 AreaFile::gUniformView = 0;
std::vector<uint32> AreaChunkRender::indices;

AreaFile::AreaFile(const std::wstring& name, FileEntryPtr file) {
	mPath = file->getFullPath();
	mModelName = std::tr2::sys::wpath(name).filename();

	mFile = file;
	sIOMgr->getArchive()->getFileData(file, mContent);
	mStream = std::make_shared<BinStream>(mContent);
}

bool AreaFile::load() {
	if (gAreaGeom == nullptr) {
		initGeometry();
	}

	uint32 signature = mStream->read<uint32>();
	if (signature != 'area') {
		return false;
	}

	uint32 version = mStream->read<uint32>();
	if (version != 0) {
		return false;
	}

	std::map<uint32, std::vector<uint8>> chunks;
	while (mContent.size() - mStream->tell() >= 8) {
		uint32 magic = mStream->read<uint32>();
		uint32 size = mStream->read<uint32>();
		if (size + mStream->tell() > mContent.size()) {
			break;
		}

		std::vector<uint8> data(size);
		mStream->read(data.data(), size);
		chunks[magic] = data;
	}

	auto itr = chunks.find('CHNK');
	if (itr == chunks.end()) {
		return false;
	}
	
	auto strm = std::make_shared<BinStream>(itr->second);
	mChunks.resize(256);
	AreaChunk ac;

	uint32 numValidChunks = 0;
	float totalHeight = 0.0f;

	uint32 lastIndex = 0;
	while (itr->second.size() - strm->tell() >= 4) {
		uint32 value = strm->read<uint32>();
		uint32 index = value >> 24;
		index += lastIndex;
		uint32 size = (value & 0xFFFFFF);
		std::vector<uint8> data(size);
		if (index >= 256) {
			break;
		}

		if (strm->tell() + size > itr->second.size()) {
			break;
		}

		strm->read(data.data(), data.size());
		memcpy(&ac, data.data(), std::min<uint32>(sizeof(AreaChunk), data.size()));
		mChunks[index] = std::make_shared<AreaChunkRender>(ac, (index % 16) * 16 * UnitSize, (index / 16) * 16 * UnitSize);
		if (mChunks[index]->getMaxHeight() > mMaxHeight) {
			mMaxHeight = mChunks[index]->getMaxHeight();
		}

		totalHeight += mChunks[index]->getAverageHeight();
		++numValidChunks;

		lastIndex = index + 1;
	}

	mAverageHeight = totalHeight / numValidChunks;

	return true;
}

void AreaFile::render(const Matrix& matView, const Matrix& matProj) {
	auto prog = gAreaGeom->getProgram();
	prog->begin();
	prog->set(gUniformProj, matProj);
	prog->set(gUniformView, matView);
	prog->end();

	for (auto& chunk : mChunks) {
		if (chunk != nullptr) {
			chunk->render();
		}
	}
}

void AreaFile::initGeometry() {
	gAreaGeom = std::make_shared<InputGeometry>();
	gAreaGeom->addElement(std::make_shared<VertexElement>(VertexSemantic::Position, 0, 3));
	auto elem = std::make_shared<VertexElement>(VertexSemantic::Color, 0, 4, DataType::Byte);
	elem->setNormalized(true);
	gAreaGeom->addElement(elem);
	gAreaGeom->setTriangleCount(18 * 18 * 2);
	gAreaGeom->setVertexCount(19 * 19);
	gAreaGeom->setVertexLayout(VertexLayout::Triangles);
	gAreaGeom->setStride(sizeof(AreaVertex));
	
	ProgramPtr prog = std::make_shared<Program>();
	prog->loadPixelShader("GLSL", "TERRAINFRAGMENT");
	prog->loadVertexShader("GLSL", "TERRAINVERTEX");
	prog->linkProgram();

	gAreaGeom->setProgram(prog);
	gAreaGeom->finalize();

	gUniformProj = prog->getUniformIndex(L"matProj");
	gUniformView = prog->getUniformIndex(L"matView");
}

void AreaFile::exportToObj() {
	std::async(std::launch::async, [this]() {
		std::wstringstream strm;
		strm << sIOMgr->getExtractionPath() << mPath << L".obj";

		std::wstring dir = std::tr2::sys::wpath(strm.str()).branch_path();

		SHCreateDirectoryEx(nullptr, dir.c_str(), nullptr);

		std::ofstream os(strm.str());

		uint32 curIndex = 0;
		for (auto& chunk : mChunks) {
			chunk->exportToObj(os, curIndex);
		}

		os.close();

		sUIMgr->asyncExtractComplete();
	});
}

AreaChunkRender::AreaChunkRender(const AreaChunk& ac, float baseX, float baseY) : mData(ac) {
	mVertexBuffer = std::make_shared<VertexBuffer>();
	mIndexBuffer = std::make_shared<IndexBuffer>();

	if (indices.size() == 0) {
		indices.resize(18 * 18 * 6);
		for (uint32 i = 0; i < 18; ++i) {
			for (uint32 j = 0; j < 18; ++j) {
				uint32 tribase = (i * 18 + j) * 6;
				uint32 ibase = i * 19 + j;
				indices[tribase] = ibase;
				indices[tribase + 1] = ibase + 1;
				indices[tribase + 2] = ibase + 20;

				indices[tribase + 3] = ibase;
				indices[tribase + 4] = ibase + 20;
				indices[tribase + 5] = ibase + 19;
			}
		}
	}

	mIndexBuffer->setIndexType(true);
	mIndexBuffer->setIndices(indices);

	float totalHeight = 0.0f;
	float lastHeight = -FLT_MAX;
	uint32 numValid = 0;
	bool first = true;

	mVertices.resize(19 * 19);
	for (uint32 y = 0; y < 19; ++y) {
		for (uint32 x = 0; x < 19; ++x) {
			auto h = ac.heightMap[y][x] & 0x7FFF;
			uint8 r = (uint8) (((h % 500) / 500.0f) * 255.0f);
			uint8 g = (uint8) (((h % 1000) / 1000.0f) * 255.0f);
			uint8 b = (uint8) (((h % 1500) / 1500.0f) * 255.0f);

			AreaVertex v;
			v.x = baseX + x * AreaFile::UnitSize;
			v.z = baseY + y * AreaFile::UnitSize;
			v.y = -2048.0f + h / 8.0f;
			v.r = r;
			v.g = g;
			v.b = b;
			v.a = 0xFF;

			if (v.y > mMaxHeight) {
				mMaxHeight = v.y;
			}

			if (first == false) {
				auto diff = std::abs(v.y - lastHeight);
				if (diff < 500.0f) {
					totalHeight += v.y;
					lastHeight = v.y;
					++numValid;
				}
			} else {
				first = false;
				totalHeight += v.y;
				++numValid;
			}

			mVertices[y * 19 + x] = v;
		}
	}

	totalHeight /= numValid;
	mAverageHeight = totalHeight;

	mVertexBuffer->setData(mVertices);
}

void AreaChunkRender::exportToObj(std::ofstream& os, uint32& index) {
	for (auto& v : mVertices) {
		os << "v " << v.x << " " << v.y << " " << v.z << std::endl;
	}

	for (uint32 i = 0; i < 18 * 18 * 2; ++i) {
		auto base = i * 3;
		os << "f " << (indices[base] + index + 1) << " " << (indices[base + 1] + index + 1) << " " << (indices[base + 2] + index + 1) << std::endl;
	}

	index += 19 * 19;
}

void AreaChunkRender::render() {
	gAreaGeom->setVertexBuffer(mVertexBuffer);
	gAreaGeom->setIndexBuffer(mIndexBuffer);

	sPipeline->applyGeometry(gAreaGeom);
	sPipeline->render();
	sPipeline->removeGeometry(gAreaGeom);
}