#include "StdAfx.h"
#include "File.h"

using namespace Awesomium;

namespace File {
	std::wstring getExtension(std::wstring file) {
		std::tr2::sys::wpath p(file);

		return p.extension().substr(1);
	}

	WebString getExtension(const WebString& file) {
		std::wstring fname(reinterpret_cast<const wchar_t*>(file.data()), file.length());
		return WebString((const wchar16*)File::getExtension(fname).c_str());
	}
}