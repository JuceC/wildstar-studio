#include "stdafx.h"
#include "Window.h"
#include "GxContext.h"
#include "ScriptManager.h"

std::wstring toUnicode(const std::string& ascii) {
	static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> cvt;

	return cvt.from_bytes(ascii);
}

BOOL WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT) {
	CoInitialize(nullptr);
	ULONG_PTR gdiToken = 0;
	Gdiplus::GdiplusStartupInput startInput;
	Gdiplus::GdiplusStartup(&gdiToken, &startInput, nullptr);

	WindowPtr wnd = std::make_shared<Window>();
	sGxCtx->init(wnd);

	sScriptMgr->initGlobalContext();

	wnd->syncRunLoop();

	Gdiplus::GdiplusShutdown(gdiToken);

	CoUninitialize();
}