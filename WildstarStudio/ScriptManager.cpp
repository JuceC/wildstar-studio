#include "stdafx.h"
#include "ScriptManager.h"
#include "Files.h"
#include "Common.h"

std::shared_ptr<ScriptManager> ScriptManager::gInstance;

ScriptManager::ScriptManager() {
	mGlobalInstance = std::make_shared<V8Instance>();
	v8::V8::SetArrayBufferAllocator(this);
}

void ScriptManager::initGlobalContext() {
	mGlobalInstance->initGlobal();

	FileLibrary::onRegister(mGlobalInstance->getGlobalScope());
	Common::onRegister(mGlobalInstance->getGlobalScope());

	mGlobalInstance->createContexts();
}

void ScriptManager::run(const std::wstring& code) {
	mGlobalInstance->runScriptGlobal(code);
	v8::V8::IdleNotification();
}

void* ScriptManager::Allocate(size_t len) {
	auto ret = new uint8[len];
	ZeroMemory(ret, len);
	return ret;
}

void* ScriptManager::AllocateUninitialized(size_t len) {
	return new uint8[len];
}

void ScriptManager::Free(void* data, size_t len) {
	delete [] (uint8*)data;
}