#include "stdafx.h"
#include "DataTable.h"

void DataTable::exportAsCsv(const std::wstring& filePath) {
	std::wofstream os(filePath);

	for (uint32 i = 0; i < mColumnHeaders.size(); ++i) {
		if (i != 0) {
			os << L";";
		}

		os << L"\"" << mColumnHeaders[i] << L"\"";
	}

	os << std::endl;

	mStream->seek(mHeader.ofsEntries + 0x60);

	std::vector<uint8> dataBuffer((uint32) mHeader.recordSize);

	bool first = true;

	for (uint32 i = 0; i < mHeader.numEntries; ++i) {
		mStream->read(dataBuffer.data(), dataBuffer.size());
		uint8* ptr = dataBuffer.data();

		bool skip = false;

		for (uint32 j = 0; j < mFieldDescs.size(); ++j) {
			if (skip == true && (j > 0 && mFieldDescs[j - 1].type == FieldType::StringTableOffset) && mFieldDescs[j].type != FieldType::StringTableOffset) {
				ptr += 4;
				skip = false;
			} else {
				skip = false;
			}

			if (j != 0) {
				os << L";";
			}

			switch (mFieldDescs[j].type) {
			case FieldType::UInt32:
				os << *(uint32*) ptr;
				ptr += sizeof(uint32);
				break;

			case FieldType::UInt64:
				os << *(uint64*) ptr;
				ptr += sizeof(uint64);
				break;

			case FieldType::Float:
				os << *(float*) ptr;
				ptr += sizeof(float);
				break;

			case FieldType::Bool:
				os << ((*(uint32*) ptr) != 0 ? L"0" : L"1");
				ptr += 4;
				break;

			case FieldType::StringTableOffset:
				{
					uint32 ofsLower = *(uint32*) ptr;
					ptr += 4;
					uint64 offset = *(uint64*) ptr;;

					skip = ofsLower == 0;
					if (skip == false) {
						offset = ofsLower | ((offset & 0xFFFFFFFF) << 32);
					}

					offset += mHeader.ofsEntries + 0x60;

					std::wstring str = L"";
					if (offset < mContent.size()) {
						str = (wchar_t*) &mContent[(uint32) offset];
					}

					str = escapeJsonString(str);
					os << L"\"" << str << L"\"";
				}
				break;
			}
		}

		os << std::endl;
	}

	os.close();
}