//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resources.rc
//
#define IDS_VERSION_STRING              101
#define IDR_GLSL1                       102
#define IDR_GLSL2                       103
#define IDR_241                         104
#define IDR_GLSL3                       105
#define IDR_GLSL4                       106
#define IDR_GLSL5                       107
#define IDD_OFFSET_DIALOG               108
#define IDR_GLSL6                       109
#define IDR_GLSL7                       110
#define IDC_EDIT1                       1001
#define IDC_BUTTON1                     1002
#define IDC_GO_BUTTON                   1002
#define IDC_RADIO1                      1003
#define IDC_RADIO2                      1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
