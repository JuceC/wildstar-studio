attribute vec3 position0;
attribute vec4 color0;

uniform mat4 matView;
uniform mat4 matProj;

varying vec4 color;

void main(void) {
	color = color0;
	gl_Position = matProj * matView * vec4(position0, 1);
}